//import libraries
const express = require('express'), socketio = require('socket.io');
const config = require('config'), PORT = config.get('socketServerPort') || 9000;
const stdin = process.openStdin();

//init express app and socketio
const app = express(); app.use(express.static(__dirname + '/public'));
const expressServer = app.listen(PORT), io = socketio(expressServer);

//Declaring custom namespace for rooms
var clearRoomTimer = null;
const ludoNamespace = io.of('/LudoGame');
ludoNamespace.on('connection', function (nsCon) {
    nsCon.on('joinTheRoom', function (roomName) {
        if (clearRoomTimer === null) {
            clearRoomTimer = setTimeout(() => {
                if (Object.keys(nsCon.adapter.sids).length > 3) return;
                nsCon.leave(roomName);
                console.log('room is dead....');
            }, 30000);
        }
        if (Object.keys(nsCon.adapter.sids).length == 4) { console.log('game is live...'); return; }
        nsCon.join(roomName, function () { if (Object.keys(nsCon.adapter.sids).length == 1) console.log('game started...'); });
    });
});